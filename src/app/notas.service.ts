import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Nota } from './modelo/nota';
import { CargadorService } from './cargador.service';

@Injectable({
  providedIn: 'root'
})
export class NotasService {

  private notas: Nota[] = [];
  private notasAleatorias: Nota[] = [];

  public avisos: BehaviorSubject<string> = new BehaviorSubject<string>('');

  public getTotalNotas(): number {
    return this.notas.length;
  }

  public emiteAviso(msj: string): void {
    console.info('Mensaje emitido: ', msj);
    this.avisos.next(msj);
  }

  public getNotasPorAula(aula: string): Nota[] {
    return this.notas.filter(nota => nota.aula.toUpperCase() === aula.toUpperCase());
  }

  public esAprobado(nota: number): boolean {
    return nota >= 5;
  }

  public borrarNota(id: number): void {
    this.borrarNotaApi(id);
  }

  public borrarNotaApi(id: number): void {
    this.http.delete('http://localhost:3000/notas/' + id).subscribe(
      datos => {
        this.notas = this.notas.filter( nota => nota.id !== id );
        this.emiteAviso('nota-borrada');
      },
      err => {
        console.log(err);
      }
    );
  }

  public anyadirNota(nota: Nota): void {
    this.anyadirNotaApi(nota);
  }

  public anyadirNotaApi(nota: Nota): void {
    this.http.post<Nota>('http://localhost:3000/notas', nota).subscribe(
      datos => {
        this.notas.push(datos);
        this.emiteAviso('nota-creada');
      }
    );
  }

  public getNotaAleatoria(aula: string): Nota {
    const n = Math.floor(Math.random() * this.notasAleatorias.length);
    const notaAleatoria = this.notasAleatorias[n];
    notaAleatoria.aula = aula;
    notaAleatoria.id = null;
    return notaAleatoria;
  }

  public getNotaMediaPorAula(aula: string): number {
    const notas: Nota[] = this.getNotasPorAula(aula);
    const totalNotas: number = notas
            .reduce( (suma: number, nota: Nota): number => nota.nota + suma, 0);
    return  totalNotas / notas.length;
  }

  public getNotaApi(id): void {
    this.http.get<Nota>('http://localhost:3000/notas/' + id).subscribe();
  }

  public getNotasApi(): void {
    const peticiones: Array<Observable<any>> = [];
    const getNotasObservable = this.http.get<Nota[]>('http://localhost:3000/notas').pipe(
      tap( datos => {
        this.notas = datos;
        this.emiteAviso('notas-cargadas');
      }),
      catchError( err => throwError('Ha ocurrido un error al solicitar las notas a la API: ' + err.message) )
    );
    peticiones.push(getNotasObservable);
    const getNotasAleatoriasObservable = this.http.get<Nota[]>('http://localhost:3000/notasAleatorias').pipe(
      tap( datos => {
        this.notasAleatorias = datos;
        this.emiteAviso('notas-aleatorias-cargadas');
      }),
      catchError( err => throwError('Ha ocurrido un error al solicitar las notas aleatorias a la API: ' + err.message) )
    );
    peticiones.push(getNotasAleatoriasObservable);

    this.cargadorService.allPeticiones(peticiones).subscribe(
      aviso => this.emiteAviso(aviso),
      err => console.error(err)
    );
  }

  constructor(private http: HttpClient, private cargadorService: CargadorService) {
    this.getNotasApi();
  }

  public existeAlumno(nombre: string, apellido: string): boolean {
    console.log(`Buscando a ${nombre} ${apellido}`);
    return this.notas.find( nota => nota.nombre === nombre && nota.apellido === apellido) ? true : false;
  }
}
