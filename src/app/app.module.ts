import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { AulaComponent } from './aula/aula.component';
import { GeneralComponent } from './general/general.component';
import { NuevaNotaComponent } from './nueva-nota/nueva-nota.component';
import { NotasComponent } from './notas/notas.component';
import { DatosAulaComponent } from './datos-aula/datos-aula.component';
import { DecimalesPipe } from './pipes/decimales.pipe';
import { DecimalPipe } from '@angular/common';
import { PantallaNotasComponent } from './pantalla-notas/pantalla-notas.component';
import { FormularioNotaComponent } from './formulario-nota/formulario-nota.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    AulaComponent,
    GeneralComponent,
    NuevaNotaComponent,
    NotasComponent,
    DatosAulaComponent,
    DecimalesPipe,
    PantallaNotasComponent,
    FormularioNotaComponent,
    LoginComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [DecimalPipe, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
