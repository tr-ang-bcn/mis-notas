import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'decimales'
})
export class DecimalesPipe implements PipeTransform {

  constructor(private decimalPipe: DecimalPipe) { }

  transform(value: any): any {
    return this.decimalPipe.transform(value, '0.0-2');
  }

}
