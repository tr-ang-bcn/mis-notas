import { Component, OnInit, Input } from '@angular/core';

import { Nota } from '../modelo/nota';
import { NotasService } from '../notas.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'notas-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit {

  @Input()
  public aula: string;

  get notas(): Nota[] {
    return this.notasService.getNotasPorAula(this.aula);
  }

  constructor(
    private notasService: NotasService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  public esAprobado(nota: number): boolean {
    return this.notasService.esAprobado(nota);
  }

  public borrar(id: number, e: Event): void {
    e.preventDefault();
    this.notasService.borrarNota(id);
  }

  public editarNota(nota: Nota): void {
    this.router.navigate(['/notas/editar', nota.id]);
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(
      datos => {
        if (datos.get('pag')) {
          console.log(datos.get('pag'));
        }
      }
    );
  }

}
