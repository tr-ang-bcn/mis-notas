import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'notas-aula',
  templateUrl: './aula.component.html',
  styleUrls: ['./aula.component.css']
})
export class AulaComponent implements OnInit {

  @Input()
  public aula: string;

  public mostrarFormulario = false;

  constructor(private router: Router) { }

  public cerrarFormulario(mensaje: string): void {
    if (mensaje === 'cerrar') {
      this.mostrarFormulario = false;
    }
  }

  public toggleFormulario(e: Event): void {
    e.preventDefault();
    this.mostrarFormulario = !this.mostrarFormulario;
  }

  public anyadirNota(e: Event): void {
    e.preventDefault();
    this.router.navigate(['notas/anyadir'], { queryParams: { aula: this.aula }});
  }

  ngOnInit() {
  }

}
