import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';

import { Nota } from '../modelo/nota';
import { NotasService } from '../notas.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'notas-nueva-nota',
  templateUrl: './nueva-nota.component.html',
  styleUrls: ['./nueva-nota.component.css']
})
export class NuevaNotaComponent implements OnInit, OnDestroy {

  @Input()
  public aula: string;

  @Output()
  public cerrar: EventEmitter<string> = new EventEmitter<string>();

  public nuevaNota: Nota;
  private tituloGeneral: string;

  constructor(private notasService: NotasService, private titleService: Title) { }

  public anyadir(): void {
    this.notasService.anyadirNota(this.nuevaNota);
    this.nuevaNota = this.notasService.getNotaAleatoria(this.aula);
    this.cerrar.emit('cerrar');
  }

  ngOnInit() {
    this.nuevaNota = this.notasService.getNotaAleatoria(this.aula);
    this.tituloGeneral = this.titleService.getTitle();
    this.titleService.setTitle(this.tituloGeneral + ' - Añadir nota de ' + this.nuevaNota.nombre);
  }

  ngOnDestroy() {
    this.titleService.setTitle(this.tituloGeneral);
  }

}
