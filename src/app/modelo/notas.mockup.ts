import { Nota } from './nota';

export const notasBD: Nota[] = [
  {
    'id': 1,
    'aula': 'A',
    'nombre': 'Paco',
    'apellido': 'Pil',
    'nota': 8
  },
  {
    'id': 2,
    'aula': 'B',
    'nombre': 'Carmen',
    'apellido': 'Sevilla',
    'nota': 7
  },
  {
    'id': 3,
    'aula': 'A',
    'nombre': 'Nino',
    'apellido': 'Bravo',
    'nota': 6.5
  },
  {
    'id': 4,
    'aula': 'C',
    'nombre': 'Lola',
    'apellido': 'Flores',
    'nota': 10
  },
  {
    'id': 5,
    'aula': 'B',
    'nombre': 'Ramón',
    'apellido': 'García',
    'nota': 5
  },
  {
    'id': 6,
    'aula': 'C',
    'nombre': 'Soraya',
    'apellido': 'Sáenz',
    'nota': 2.5
  },
  {
    'id': 7,
    'aula': 'A',
    'nombre': 'Isabel',
    'apellido': 'La Católica',
    'nota': 4
  },
  {
    'id': 8,
    'aula': 'C',
    'nombre': 'Chiquito',
    'apellido': 'de la Calzada',
    'nota': 9.5
  },
  {
    'id': 9,
    'aula': 'B',
    'nombre': 'Peter',
    'apellido': 'Griffin',
    'nota': 8.5
  }
];

export const notasAleatorias: Nota[] = [
  {
    'id': 100,
    'aula': 'A',
    'nombre': 'Raquel',
    'apellido': 'Meroño',
    'nota': 8.5
  },
  {
    'id': 101,
    'aula': 'B',
    'nombre': 'Pablo',
    'apellido': 'Carbonell',
    'nota': 7
  },
  {
    'id': 102,
    'aula': 'C',
    'nombre': 'Pepe',
    'apellido': 'Begines',
    'nota': 9
  },
  {
    'id': 103,
    'aula': 'A',
    'nombre': 'Bob',
    'apellido': 'Pop',
    'nota': 7
  },
  {
    'id': 104,
    'aula': 'B',
    'nombre': 'Marc',
    'apellido': 'Giró',
    'nota': 5
  },
  {
    'id': 105,
    'aula': 'C',
    'nombre': 'Cristina',
    'apellido': 'Rosenvinge',
    'nota': 3
  },
  {
    'id': 110,
    'aula': 'A',
    'nombre': 'Boticaria',
    'apellido': 'García',
    'nota': 4.5
  },
  {
    'id': 111,
    'aula': 'B',
    'nombre': 'Antonio',
    'apellido': 'Luque',
    'nota': 1
  },
  {
    'id': 112,
    'aula': 'C',
    'nombre': 'José María',
    'apellido': 'Sant Feliu',
    'nota': 6.5
  }
];
