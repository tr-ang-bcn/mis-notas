import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PantallaNotasComponent } from './pantalla-notas/pantalla-notas.component';
import { FormularioNotaComponent } from './formulario-nota/formulario-nota.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuardGuard } from './auth-guard.guard';

const rutas: Routes = [
  { path: 'notas', component: PantallaNotasComponent, canActivate: [ AuthGuardGuard ] },
  { path: 'notas/anyadir', component: FormularioNotaComponent, data: { tipo: 'examen' }, canActivate: [ AuthGuardGuard ] },
  { path: 'notas/editar/:id', component: FormularioNotaComponent, data: { tipo: 'práctica' }, canActivate: [ AuthGuardGuard ] },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/notas', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule(
  {
    imports: [
      RouterModule.forRoot(rutas)
    ],
    exports: [ RouterModule ]
  }
)
export class AppRoutingModule { }
