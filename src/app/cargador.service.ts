import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CargadorService {

  private terminador: Subject<void> = new Subject();

  public allPeticiones(peticiones): Observable<any> {
    return new Observable(
      observer => {
        peticiones.forEach((peticion, i) => {
          peticion.pipe(
            takeUntil(this.terminador)
          )
          .subscribe(
            datos => {
              emitOnLast(i);
            },
            err => {
              emitOnLast(i);
              console.error(err);
            }
          );
          const emitOnLast = (n: number) => {
            if (n === peticiones.length - 1) {
              observer.next('todo-cargado');
              this.terminador.next();
            }
          };
        });
      }
    );
  }

  constructor() { }
}
