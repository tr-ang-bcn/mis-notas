import { Component, OnInit, Input } from '@angular/core';
import { NotasService } from '../notas.service';

@Component({
  selector: 'notas-datos-aula',
  templateUrl: './datos-aula.component.html',
  styleUrls: ['./datos-aula.component.css']
})
export class DatosAulaComponent implements OnInit {

  @Input()
  public aula: string;

  public notaMaxima = 6;
  public numAprobados = 1;
  public porcentajeAprobados = 33.333333333337;
  public numSuspensos = 2;
  public porcentajeSuspensos = 66.66666666663;

  get notaMedia(): number {
    return this.notasService.getNotaMediaPorAula(this.aula);
  }

  constructor(private notasService: NotasService) { }

  public clasesCarita(nota: number): Object {
    return {
      'fa-smile': this.notasService.esAprobado(nota),
      'fa-frown': !this.notasService.esAprobado(nota),
      'icono-ok': this.notasService.esAprobado(nota),
      'icono-no-ok': !this.notasService.esAprobado(nota)
    };
  }

  ngOnInit() {

  }

}
