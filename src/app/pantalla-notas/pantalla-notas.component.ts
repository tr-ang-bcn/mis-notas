import { Component, OnInit } from '@angular/core';
import { NotasService } from '../notas.service';

@Component({
  selector: 'notas-pantalla-notas',
  templateUrl: './pantalla-notas.component.html',
  styleUrls: ['./pantalla-notas.component.css']
})
export class PantallaNotasComponent implements OnInit {

  public cargado;

  constructor(private notasService: NotasService) { }

  private toggleCargado(): void {
    this.cargado = true;
  }

  ngOnInit() {
    if (this.notasService.getTotalNotas() === 0) {
      this.cargado = false;
      this.notasService.avisos.subscribe( mensaje => {
        if (mensaje === 'notas-cargadas') {
          this.toggleCargado();
        }
      });
    } else {
      this.cargado = true;
    }
  }


}
