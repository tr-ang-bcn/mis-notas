import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Nota } from '../modelo/nota';
import { NotasService } from '../notas.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'notas-formulario-nota',
  templateUrl: './formulario-nota.component.html',
  styleUrls: ['./formulario-nota.component.css']
})
export class FormularioNotaComponent implements OnInit {

  public pulsaciones: Subject<{}> = new Subject<{}>();
  public nueva = true;
  public nuevaNota: Nota = {
    id: 0,
    nombre: '',
    apellido: '',
    nota: null,
    aula: ''
  };
  public mostrarAviso = false;
  public existe = false;

  constructor(private notasService: NotasService, private route: ActivatedRoute, private router: Router) { }

  public cambiaNombreApellidos(): void {
    this.pulsaciones
    .next({ nombre: this.nuevaNota.nombre, apellido: this.nuevaNota.apellido });
  }

  public submitForm(form): void {
    this.notasService.anyadirNota(this.nuevaNota);
    form.reset();
    this.mostrarAviso = true;
    setTimeout(() => {
      this.router.navigate(['/notas']);
    }, 2000);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      parametros => {
        if (parametros.get('id')) {
          this.nueva = false;
        }
      }
    );
    this.route.data.subscribe(
      datos => {
        if (datos) {
          console.log(datos);
        }
      }
    );
    this.route.queryParamMap.subscribe(
      parametros => this.nuevaNota.aula = parametros.get('aula')
    );
    this.pulsaciones.pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe(
      datos => {
        this.existe = this.notasService.existeAlumno(this.nuevaNota.nombre, this.nuevaNota.apellido);
      }
    );
  }

}
