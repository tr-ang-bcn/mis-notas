import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotasService } from './notas.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'notas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
