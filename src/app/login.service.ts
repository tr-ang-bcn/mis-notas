import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private logueado = false;
  public urlRedireccion = '';

  constructor(private cookiesService: CookieService, private router: Router) {
    if (this.cookiesService.get('logueado') === '1') {
      this.logueado = true;
    }
  }

  public estaLogueado(): boolean {
    return this.logueado;
  }

  public loguea(): void {
    this.logueado = true;
    this.cookiesService.set('logueado', '1');
    this.router.navigate([this.urlRedireccion]);
  }
}
